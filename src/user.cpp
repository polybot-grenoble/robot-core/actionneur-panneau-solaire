#include "actionneur_panneau_solaire.hpp"
#include "core/core.hpp"

enum Commands { CMD_STOP = 0, CMD_SET_TEAM = 1, CMD_GET_TEAM = 2 };

void Talos_initialisation(){
    setupPin();
    Core_setMinimumLoopPeriod(1000);
}

void Talos_loop() {
}

void Talos_onError() {
    setTeam(STOP);
    Core_softResetIfConnected();
}

void Core_priorityLoop() {
}

void Hermes_onMessage(Hermes_t *hermesInstance, uint8_t sender,
                      uint16_t command, uint32_t isResponse) {
    HermesBuffer *buf;
    if (Hermes_getInputBuffer(hermesInstance, sender, command, &buf) !=
        HERMES_OK) {
        return;
    }

    switch ((Commands)command) {
        case CMD_STOP:
            setTeam(STOP);
            break;
        case CMD_SET_TEAM: {
            char team;
            Hermes_rewind(buf);
            Hermes_get(buf, &team);

            setTeam((Team)team);
            break;
        }
        case CMD_GET_TEAM: {
            Hermes_send(hermesInstance, sender, command, true, "c", (char)getTeam());
            break;
        }

        default:
            // TODO
            // Core_log()
            break;
    }
}

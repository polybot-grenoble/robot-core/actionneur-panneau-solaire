#include "actionneur_panneau_solaire.hpp"

#include <Arduino.h>

#include "core/unit.hpp"

Team currentTeam = STOP;
const pin PIN_BLUE = A1;
const pin PIN_YELLOW = A0;
const pin PIN_USER_LED = D13;

void setupPin() {
    pinMode(PIN_BLUE, OUTPUT);
    pinMode(PIN_YELLOW, OUTPUT);
    pinMode(PIN_USER_LED, OUTPUT);

    setTeam(STOP);
}

void setTeam(Team team) {
    digitalWrite(PIN_BLUE, team == BLUE);
    digitalWrite(PIN_YELLOW, team == YELLOW);
    digitalWrite(PIN_USER_LED, team != STOP);
    currentTeam = team;
}

Team getTeam() { return currentTeam; }

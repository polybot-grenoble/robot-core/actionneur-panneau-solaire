# Actionneur panneau solaire

## Commands

| id | name | parameters | response |
|------------|------|------------|----------|
|0|stop| none | none |
|1|set_team| [Stop: `'S'`, Blue: `'B'`, Yellow: `'Y'`] | none |
|2|get_team| none | [Stop: `'S'`, Blue: `'B'`, Yellow: `'Y'`] |

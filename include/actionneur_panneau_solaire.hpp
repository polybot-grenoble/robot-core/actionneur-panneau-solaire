
enum Team { STOP = 'S', BLUE = 'B', YELLOW = 'Y' };

void setupPin();

void setTeam(Team);
Team getTeam();

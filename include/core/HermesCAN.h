#ifndef __POLYBOT_HERMES_CAN__
#define __POLYBOT_HERMES_CAN__

#include <Arduino.h>
#include <Hermes.h>

#ifndef HERMES_CAN_MODE
#define HERMES_CAN_MODE CAN_MODE_NORMAL
#endif

#ifndef BROADCAST_ID
#define BROADCAST_ID 0xFF
#endif

/*Defines which interrupts to listen for.*/
#define CAN_ITS                                                         \
    CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_RX_FIFO1_MSG_PENDING |         \
        CAN_IT_TX_MAILBOX_EMPTY | CAN_IT_ERROR | CAN_IT_ERROR_PASSIVE | \
        CAN_IT_ERROR_WARNING | CAN_IT_BUSOFF

typedef enum {
    HERMES_CAN_OK,   /** It's all good. */
    HERMES_CAN_ERROR /** There is a CAN related problem. Check HAL errors. */
} HermesCANReturnCode;

/**
 * Sets up the HAL CAN handle and starts the CAN controller.
 * @param hermes Main hermes instance.
 * @param hcan1 HAL CAN handle.
 */

HermesCANReturnCode HermesCAN_setup(Hermes_t *hermes, CAN_HandleTypeDef *hcan1);

/**
 * Sets up the CAN ITs and starts listening for them.
 * @param hcan1 HAL CAN handle.
 */
HermesCANReturnCode HermesCAN_setupIt(CAN_HandleTypeDef *hcan1);

/**
 * Checks if there are any messages waiting to be send in the hermes instance.
 * If so, the first one is transmitted to the CAN handle.
 * @param hermes Main Hermes instance.
 * @param hcan1 HAL CAN handle.
 */
HermesCANReturnCode HermesCAN_processTX(Hermes_t *hermes,
                                        CAN_HandleTypeDef *hcan1);

/**
 * Checks if the CAN handle has received any messages.
 * If so, the first one is gifted to the hermes instance.
 * @param hermes Main Hermes instance.
 * @param hcan1 HAL CAN handle.
 * @param fifo The fifo which received the message.
 */
HermesCANReturnCode HermesCAN_processRX(Hermes_t *hermes,
                                        CAN_HandleTypeDef *hcan1,
                                        uint32_t fifo);
#endif